module.exports = {
  siteName: 'Test',
  siteUrl: 'https://liimee.gitlab.io/testing',
  plugins: [],
  pathPrefix: '/testing',
  outputDir: 'public'
}